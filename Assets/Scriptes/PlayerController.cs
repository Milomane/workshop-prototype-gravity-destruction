﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb;

    public float jumpForce;
    public float speed;
    
    public int maxJump = 2;
    int jump;

    public float friction = .25f;

    public Transform feet;
    public Transform spriteTransform;

    public int coinCount;

    public TextMeshProUGUI textTimer;
    private bool timerRun = true;
    private float timer = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        jump = maxJump;
    }

    // Update is called once per frame
    void Update()
    {
        if (timerRun)
            timer += Time.deltaTime;
        
        textTimer.text = Mathf.FloorToInt(timer / 60) + "m : " + Mathf.FloorToInt(timer - Mathf.FloorToInt(timer / 60) * 60) + "s";
        
        RaycastHit2D hit;
        if (hit = Physics2D.Raycast(feet.position, transform.right * .05f, .05f))
        {
            rb.velocity -= rb.velocity * friction;
            
            jump = maxJump;
            
            if (Input.GetKey(KeyCode.Q))
            {
                rb.AddForce(-transform.up * speed);
                spriteTransform.localScale = new Vector3(-.6f, .6f, .6f);
            }

            if (Input.GetKey(KeyCode.D))
            {
                rb.AddForce(transform.up * speed);
                spriteTransform.localScale = new Vector3(.6f, .6f, .6f);
            }
        }
        

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (jump > 0)
            {
                jump--;
                rb.AddForce(-transform.right * jumpForce, ForceMode2D.Impulse);
            }
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    public void StopTimer()
    {
        timerRun = false;
        textTimer.color = Color.green;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawRay(feet.position, transform.right*.05f);
    }
}
