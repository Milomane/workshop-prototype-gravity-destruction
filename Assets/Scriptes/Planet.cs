﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Planet : MonoBehaviour
{
    public int planetRadiusInTile;
    public Tilemap tilemap;
    public TilemapCollider2D tilemapCollider;

    public TileBase tile;

    public Transform halo;
    
    void Start()
    {
        halo.localScale = new Vector3((float)planetRadiusInTile/64, (float)planetRadiusInTile/64, 1);
    }

    void Update()
    {
        
    }

    public void Explosion(Vector3 explosionLocation, int radiusInTile)
    {
        Debug.Log("OneLessTile" + gameObject.name + "    " + radiusInTile);
        
        for (int x = -(int) radiusInTile; x < radiusInTile; x++)
        {
            for (int y = -(int) radiusInTile; y < radiusInTile; y++)
            {
                Debug.Log("OneLessTile");
                
                Vector3Int tilePos = tilemap.WorldToCell(explosionLocation + new Vector3((float)x*.02f, (float)y*.02f, 0));

                if (new Vector2((float)x, (float)y).magnitude <= radiusInTile)
                {
                    if (tilemap.GetTile(tilePos))
                    {
                        DestroyTile(tilePos);
                    }
                }
            }
        }
    }

    void DestroyTile(Vector3Int pos)
    {
        tilemap.SetTile(pos, null);
    }

    public void DrawTile(Vector3Int pos)
    {
        tilemap.SetTile(pos, tile);
    }
}
