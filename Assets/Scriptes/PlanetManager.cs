﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetManager : MonoBehaviour
{
    public int maxPlanetTileSize = 64;

    public Planet[] allPlanet;

    void Start()
    {
        PlaceAllPlanetTile();
    }
    void Update()
    {
        
    }

    public GameObject[] FindAllPlanet()
    {
        GameObject[] planets = new GameObject[allPlanet.Length];

        for (int i = 0; i < allPlanet.Length; i++)
        {
            planets[i] = allPlanet[i].gameObject;
        }
        
        return planets;
    }
    
    public void PlaceAllPlanetTile()
    {
        for (int x = -(int)maxPlanetTileSize; x < maxPlanetTileSize; x++)
        {
            for (int y = -(int)maxPlanetTileSize; y < maxPlanetTileSize; y++)
            {
                foreach (Planet planet in allPlanet)
                {
                    if (new Vector2(x, y).magnitude <= planet.planetRadiusInTile)
                    {
                        planet.DrawTile(new Vector3Int(x, y, 0));
                    }
                }
            }
        }
    }
}
