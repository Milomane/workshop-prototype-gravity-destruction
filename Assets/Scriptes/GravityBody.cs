﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityBody : MonoBehaviour
{
    public GameObject[] planets;

    public float maxGravity;
    public float maxGravityDist;

    public float lookAngle;
    public Vector3 lookDirection;

    public Rigidbody2D rb;

    void Start()
    {
        planets = FindObjectOfType<PlanetManager>().FindAllPlanet();
    }

    void Update()
    {
        GameObject nearestPlanet = null;
        float maxForce = 0;

        for (int i = 0; i < planets.Length; i++)
        {
            GameObject planet = planets[i];
            
            float dist = Vector2.Distance(planet.transform.position, transform.position);
            

            Vector3 v = planet.transform.position - transform.position;
            float force = (1.0f - dist / (maxGravityDist * planet.transform.localScale.x)) * maxGravity * rb.mass;

            if (dist < maxGravityDist * planet.transform.localScale.x)
            {
                rb.AddForce(v.normalized * force);
            }

            if (force > maxForce)
            {
                maxForce = force;
                nearestPlanet = planet;
            }
        }

        if (nearestPlanet != null)
        {
            lookDirection = nearestPlanet.transform.position - transform.position;
            lookAngle = Mathf.Atan2(lookDirection.y, lookDirection.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, 0f, lookAngle);
        }
    }
}
